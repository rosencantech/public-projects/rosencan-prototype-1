
# ![Rosencan](https://gitlab.com/rosencantech/assets/raw/master/rcc163.png)

Rosencan will be a fully featured, interactive classroom game designed to capture the attention of students all around the world to motivate them into studying more often.

## Prototype Version 1

This prototype version implements **very** basic gameplay features, and displays little to no final design elements. The game loading system is also in very early stages, and should also be taken very lightly. This prototype is designed to simply demonstrate the basics of server and client communications in the game, and how they can be used to dynamically edit a single page through the game.

### Credits

|Picture|Person|
|---|---|
| ![ben@devnull](https://secure.gravatar.com/avatar/6e34018d6d5033285b52a20ec8ef19e6?s=90&d=identicon)  |**ben@devnull (Ben)** - Developer and Creator  |